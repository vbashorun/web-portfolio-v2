//This script handles the animation for the graph(s)
$(document).ready(function() 
{
    $(window).on('resize', function()
    {
        size(false)
    });

    // set up the timeout variable
    var t;

    // setup the sizing function,
    // with an argument that tells the chart to animate or not
    function size(animate)
    {
        // If we are resizing, we don't want the charts drawing on every resize event.
        // This clears the timeout so that we only run the sizing function
        // when we are done resizing the window
        clearTimeout(t);

        // This will reset the timeout right after clearing it.
        t = setTimeout(function()
        {
            $("canvas").each(function(i,el)
            {
                // Set the canvas element's height and width to it's parent's height and width.
                // The parent element is the div.canvas-container
                $(el).attr({ "width":$(el).parent().width(), "height":$(el).parent().outerHeight() });
            });

            // kickoff the redraw function, which builds all of the charts.
            redraw(animate);

            // loop through the widgets and find the tallest one, and set all of them to that height.
            var m = 0;
            // we have to remove any inline height setting first so that we get the automatic height.
            /*$(".widget").height("");
            $(".widget").each(function(i,el){ m = Math.max(m,$(el).height()); });
            $(".widget").height(m);*/

        }, 100); // the timeout should run after 100 milliseconds
    }

    //handles the the chart animation
    function redraw(animation)
    {
        var data = {
            
                        labels : ["HTML","CSS","JavaScript","PHP","","Photoshop","Databases" ,"Design"],
                        datasets : [
                                        {
                                            fillColor : "rgba(250, 197, 95, 0.73)",
                                            strokeColor : "#ff9300",
                                            pointColor : "#ff9300",
                                            pointStrokeColor : "#fff",
                                            data : [100,90,75,50,0,70,30,60]
                                        }
                                    ]
                   };

        var options = { 
                        scaleStartValue: null,
                        pointLabelFontColor: "#fac55f",
                        pointLabelFontSize: 14,
                        scaleLineColor: "white",
                        angleLineColor: "white"
                      };
        
        if (!animation)
        {
            options.animation = false;
        } 
        else 
        {
            options.animation = true;
        }

        //Get the context of the canvas element we want to select
        var ctx = document.getElementById("myChart").getContext("2d");
        //draw the chart
        var myNewChart = new Chart(ctx).Radar(data,options);;
    }

    size(true); // this kicks off the first drawing;
});