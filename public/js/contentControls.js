var clicked = document.getElementById("web");
    clrGray = "#484848",
    clrOrange = "#fac55f",
    shadowOut = "0px 2px 2px 0.5px rgba(0, 0, 0, 0.75)",
    shadowIn = "0px 2px 5px 0.5px rgba(0, 0, 0, 0.75) inset",
    loadAnimation = document.getElementById("content").innerHTML; // for holding load animation code;

var modal = [document.querySelector('.modalBG'), document.querySelector('.modal-wrap')];

function initialize()
{
    loadProjects("web");
}

//animates the clicked category button and load appropriate content
function showContent(element)
{
    //show loading animation
    document.getElementById("content").innerHTML = loadAnimation;
    showHideArrow("show");
    
    var caller = document.getElementById(element.id);

    //change the clicked
    caller.style.color=clrOrange;
    caller.style.backgroundColor=clrGray;
    caller.style.boxShadow=shadowIn;

    //restore the previously clicked
    clicked.style.color=clrGray;
    clicked.style.backgroundColor="#fff";
    clicked.style.boxShadow=shadowOut;

    //store currently clicked button
    clicked = caller;
    

    //load content with AJAX
    loadProjects(caller.id);
}

// display link to project description on hover
function showLink(element)
{
    element.style.opacity=1;
}

function hideLink(element)
{
    element.style.opacity=0;
}

/////////////////////////////////////////               Modal Controls                    ///////////////////////////////

function hasClass(elem, className) {
	return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

function addClass(elem, className) {
    if (!hasClass(elem, className)) {
    	elem.className += ' ' + className;
    }
}

function removeClass(elem, className) {
	var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
	if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}


function showModal(element)
{  
    //clear out current contents of modal
    document.querySelector(".content-wrap").innerHTML = loadAnimation;
    
    
    //attach the 'reveal' class to the appropriate elements
    for(var x = 0; x < 2; x++)
    {
        addClass(modal[x], 'md-active');
    }
    
    // attach Remove Modal function to elements 
    document.getElementById('close').onclick = removeModal;
    
    //Remove modal by clicking on background
    document.querySelector('.modalBG').onclick = removeModal;
    
    //load the modal content
    loadModal(element);
}

function removeModal()
{
    for(var x = 0; x < 2; x++)
    {
        removeClass(modal[x], 'md-active');
    }  
}

    
/*document.getElementById('md-click').onclick = function() {
    addClass(modal, 'md-active');
}

// Remove Modal
document.getElementById('close').onclick = function() {
    removeClass(modal, 'md-active');
}*/



/////////////////////////////////////////               Scroll Up Button                    ///////////////////////////////
window.onscroll = scrollFunction;
var isShowing = false;

function scrollFunction() 
{
    var sentinel = document.getElementById("row2"),
        target = document.getElementById("scrollWrap");
    
    //document.getElementById("scrollView").innerHTML="Postion is at" + getPosition(chosenElement).y;
    if (getPosition(sentinel).y < 0 && isShowing == false)
    {
        target.style.visibility = "visible";
        target.style.opacity = 0.5;
        isShowing = true;
    }

    else if (getPosition(sentinel).y > 0 && isShowing == true)
    {
        target.style.visibility = "hidden";
        target.style.opacity = 0;
        isShowing = false;
    }
}

function getPosition(element) 
{
    var xPosition = 0;
    var yPosition = 0;

    while(element) 
    {
        xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
        element = element.offsetParent;
    }

    return { x: xPosition, y: yPosition };
}


////////////////////////////                                  AJAX                    //////////////////////////////////////////

function loadProjects(tagName)
{
    var xmlhttp;
    
    if (window.XMLHttpRequest)
    {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            //allow animation to fade
            showHideArrow("hide");
            
            //allow animation to fade out before placing content
            window.setTimeout(function()
                              {
                                  //load content
                                  document.getElementById("content").innerHTML=xmlhttp.responseText;
                                  
                                  //fade projects into view
                                  var targets = document.getElementsByClassName("projects");
                                  window.setTimeout(function()
                                                    {
                                                          for (var x = 0; x < targets.length; x++)
                                                          {
                                                              targets[x].style.opacity = 1;
                                                          }
                                                    }, 100);
                              }, 500);
        }
    }

    //set up request and use unique id to avoid cached result
    xmlhttp.open("GET","../scripts/projectLoad.php?tag=" + tagName,true);
    xmlhttp.send();
}

function loadModal(element)
{
    var xmlhttp;
    
    if (window.XMLHttpRequest)
    {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.querySelector(".content-wrap").innerHTML =xmlhttp.responseText;
        }
    }

    //set up request and use unique id to avoid cached result
    xmlhttp.open("GET","../scripts/modalLoad.php?articleID=" + element.id,true);
    xmlhttp.send();
}

////////////////////////////                                  Loading Animation                    //////////////////////////////////////////

function showHideArrow(state) {
    var target = document.querySelector(".load-container");
    
    switch(state)
    {    
        case "hide":
            target.style.opacity = 0;
            target.style.visibility = "hidden";
            break;
            
        default:
            target.style.visibility = "visible";
            target.style.opacity = 1;
    }  
}