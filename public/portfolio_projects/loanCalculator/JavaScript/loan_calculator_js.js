// JavaScript Document

var maxAmount, 
		maxDuration,
		interestRate,
		rateHelper, 
		userAmount, 
		userDuration,
		userRate,
		payStyle = 12,
		valid = true;
			
    function validateInput()
	{	
	    userAmount = document.getElementById("loan_amount").value, 
		userDuration = document.getElementById("loan_duration").value,
		userRate = document.getElementById("prime_rate").value;
		
		var type = document.getElementById("loan_type").value; 
			
		//Set the max values
		switch (type)
		{
			case "residential":
			  maxAmount = 500000;
			  maxDuration = 30;
			  rateHelper = 0.03;
			  break;
			
			case "commercial":
			  maxAmount = 150000000;
			  maxDuration = 10;
			  rateHelper = 0.02;
			  break;
			
			case "farm":
			  maxAmount = 1500000;
			  maxDuration = 25;
			  rateHelper = 0.015;
			  break;
		}

		//validate Loan amount
		if (userAmount > maxAmount || userAmount < 0 || userAmount == "")
		{
		  valid = false;
		  alert("Amount must be between 1 and " + maxAmount + ".");
		  clearField(document.getElementById("loan_amount"));
		}
  
		//vaidate Loan duration
		if (userDuration > maxDuration || userDuration < 1 || userDuration == "")
		{
		  valid = false;
		  alert("Number of years must be between 1 and " + maxDuration + ".");
		  clearField(document.getElementById("loan_duration"));
		}
  
		//validate Prime rate
		if (userRate < 0 || userRate == "")
		{
		  valid = false;
		  alert("Prime rate percentage must be greater than 0."); 
		  clearField(document.getElementById("prime_rate"));
		}
  
		//display payment
		if (valid == true) 
		{
			var primeRate = userRate/100;
			
			//set interest rate based on prime rate and type of loan
			interestRate = primeRate + rateHelper;
						   
			//reduce interest rate by 0.5% if loan length is less than half of maximum
			if (userDuration < (maxDuration/2))
			  interestRate -= (interestRate * 0.005);
			  
			if (userDuration >= 1 && userDuration <= 5)
			  document.getElementById("pay_display").style.color="red";
			else if (userDuration >= 6 && userDuration <= 10)
			  document.getElementById("pay_display").style.color="green";
			else
			  document.getElementById("pay_display").style.color="blue";
						   
			document.getElementById("pay_display").innerHTML=("$" + parseFloat(processLoan()));
					
		}
		

	}//END validateInput()

	function processLoan()
	{
		var powValue = (1/Math.pow(1+interestRate,(payStyle * userDuration))),
		    payment = Math.round(userAmount * (interestRate/(1-powValue))*100);
		return (payment/100);
	}
	
	function payStylePicked(element)
	{
		if (element.value == "monthly")
		  payStyle = 12;
		else
		  payStyle = 24;
	}
	
	function clearField(textField)
	{
		textField.value="";
	}
		
	function clearInput()
	{
		document.getElementById("loan_amount").value="";
		document.getElementById("loan_duration").value="";
		document.getElementById("prime_rate").value="";
                document.getElementById("pay_display").innerHTML="";
	}
	
	function launchCalc()
	{
		window.open("Pop-Up Calculator.html","_blank","height=500, width=400");
	}	