// JavaScript Document

var displayString,
      op1, op2,
	  pendingOp = null,
	  newNumber = true,
	  evaluated = false,  //state variable shows if "=" has just been pressed
      inNumber = false;    //helps to determine whether a number has just been entered
  
  function keyPress(key)
  {
	  //Button event handling test
	  //alert("keyPress called. key equals " + key + ".");
	  
	  switch (key)
	  {
		  case "CE":
		    clearAll();
			break;
		  
		  case "Backspace":
		    backSpace();
			break;
		 
		  case "=":
		    if (pendingOp != null)
			{
		      op2 = parseInt(getDisplay());
			  evaluateExpression(pendingOp);
			  pendingOp = null;
			  inNumber = true;
			  evaluated = true;
			}		
			//execute unary operation
			else if (op2 == null)
			{
				op2 = op1;
				evaluateExpression(pendingOp);
			}
			break;
		  
		  case "OFF":
		    this.close();
		    break;
		  
		  default:
		    //The default case is either a number or operator key being pressed
		    if (parseInt(key) >= 0 && parseInt(key) <= 9)
			{
			  if (evaluated == true || pendingOp != null && newNumber == true)
			  {
				  clearDisplay();
				  newNumber = false;
				  
				  /* this allows any new numbers to overwrite the result of an 
				     evaluated expression (after the "=" has been pressed) instead of appending to it */
				  evaluated = false;
			  }
			  
			  //If this is a new expression, we don't want to keep the cleared-state Zero
			  if (getDisplay() == "0")
			    displayString = key;
			  else
			    displayString += key;
				
			  setDisplay();
			  inNumber = true;
			}
			
			//operator catcher
			else if (key == "+" || key == "-" || key == "/" || key == "*")
			{
				if (pendingOp != null)
				{
					op2 = parseInt(getDisplay());
					evaluateExpression(pendingOp);
				}
				
				if (inNumber == true)
				{
				  op1 = parseInt(getDisplay());    //catch operand 1
				  pendingOp = key;
				  inNumber = false;     //make sure only numbers follow the operator
				  newNumber = true;     //this will allow the calculator to get rid of the old number
				}
			}
			  
	  }//END switch
  }//END keyPress
  
  function evaluateExpression(operator)
  {
	  switch (operator)
	  {
		  case "+":
		    displayString = "" + (op1 + op2);	
		    break;
		  
		  case "-":
		    displayString = "" + (op1 - op2);
		    break;
		  
		  case "*":
		    displayString = "" + (op1 * op2);
		    break;
		  
		  case "/":
		    displayString = "" + (op1 / op2);
		    break;
	  }
	  
	  setDisplay();
  }
  
  function getDisplay()
  {
	  return document.getElementById("display").innerHTML;
  }
  
  function setDisplay()
  {
	  document.getElementById("display").innerHTML = displayString;
  }
  
  function clearDisplay()
  {
	  displayString = "0";
	  setDisplay();
  }
  
  function clearAll()
  {
	  op1 = null;
	  op2 = null;
	  pendingOp = null,
	  newNumber = true,
      inNumber = true; 
	  displayString = "0";
	  
	  setDisplay();
  }
  
  function backSpace()
  {
	  displayString = displayString.substr(0, displayString.length-1);
	  
	  //Get rid of all whitespace at the end, if any
	  if (displayString.charAt(displayString.length-1) == " ")
	    displayString = displayString.substr(0, displayString.length-1);
		
	  if (displayString == "") 
	    clearDisplay(); //prevents user from backspacing everything completely off the screen
	  else
	    setDisplay();
  }