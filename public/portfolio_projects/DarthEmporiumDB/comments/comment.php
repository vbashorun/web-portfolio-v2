<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Comments Page</title>
    <link rel="stylesheet" type="text/css" href="">
    
    <script>
        function verifyFeedback()
        {
            //make sure fields aren't empty
            if (document.getElementById("name").value == "")
                return false;
            else if (document.getElementById("email").value == "")
                return false;
            else if (document.getElementById("feedback").value == "")
                return false;
            else
                return true;
        }
    </script>
    
</head>

<body>
    <div>
        <?php include '../header/header.php'; ?>
    </div>
    
    <h2>Customer Feedback</h2>
    
    <form action="processComment.php" method="post">
        <p>Your name:<br/>
        <input type="text" id="name" name="name" size="40" /></p>

        <p>Your email address:<br/>
        <input type="text" id="email" name="email" size="40" /></p>

        <p>Your feedback:<br/>
        <textarea id="feedback" name="feedback" rows="8" cols="40" wrap="virtual" /></textarea></p>

        <p><input type="submit" value="Send Feedback" onClick="verifyFeedback()" /></p>
    </form>
</body>
</html>