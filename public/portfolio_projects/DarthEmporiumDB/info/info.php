<?php
    //database variables
    $dbHost = "localhost";
    $dbName = "vader";  
    $dbUser = "root";
    $dbPass = "password";
    $getProducts = "SELECT prodName, prodNbr FROM products";

    //connect to database
    $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
    //check the connection
    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL:".mysqli_connect_error();

    //select the database
    mysql_select_db ($dbName);

    $result = mysqli_query($con, $getProducts);

?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Vader's Emporium</title>

</head>

<body>
    <h3>Supplier's Page</h3>
    <form action="processInfo.php" method="post">
        <table>
            <tr>
                <td>Which table would you like to see?</td>
                <td>
                    <select name="tableSelection">
                        <option value="customers" >Customers</option>
                        <option value="orders" >Orders</option>
                        <option value="products" >Products</option>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>Add <input type="text" name="addThisMany" placeholder="number of items" /> unit(s) to item:</td>
                <td>
                    <select name="itemSelection">
                        <?php
                            while ($row = mysqli_fetch_array($result))
                            {
                                echo "<option value=\"".$row['prodNbr']."\" >".$row['prodName']."</option><br/>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr><td colspan="2"><input type="submit" value="Submit"/></td></tr>
        </table>
    </form>   
</body>
</html>