<?php
    //database variables
    $dbHost = "localhost";
    $dbName = "vader";  
    $dbUser = "root";
    $dbPass = "password";
    $sqlDisplayOrders = "SELECT * FROM orders";
    $sqlDisplayCustomers = "SELECT * FROM customer";
    $sqlDisplayProducts = "SELECT * FROM products";
    $sqlGetQty = "SELECT qtyHand, prodName FROM products WHERE prodNbr = '$itemSelected'";

    //connect to database
    $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
    //check the connection
    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL:".mysqli_connect_error();

    //select the database
    mysql_select_db ($dbName);

    $tableSelection = $_POST['tableSelection'];
    $addThisMany = intval($_POST['addThisMany']);
    $itemSelected = intval($_POST['itemSelection']);

    function displayCust($result)
    {
        while ($row = mysqli_fetch_array($result))
        {
            echo "Cust #: ".$row['custNbr']."\tFirst Name: ".$row['fname']."\tLast Name: "
                  .$row['lname']."\tUsername: ".$row['userid']."\tPassword: ".$row['password']."\tEmail: "
                  .$row['email']."\tAddress: ".$row['streetAddress']."\t".$row['city']."\t"
                  .$row['residentState']."\t".$row['zip']."<br/>";
        }
    }

    function displayOrd($result)
    {
        while ($row = mysqli_fetch_array($result))
        {
            echo "Order: ".$row['orderNbr']."\tDate: "
                  .$row['orderDate']."\tCustomer: ".$row['custNbr']."\tProduct #: "
                  .$row['prodNbr']."\tQuantity: ".$row['quantity']."<br/>"; 
        }
    }

    function displayProd($result)
    {
        while ($row = mysqli_fetch_array($result))
        {
            echo "Prod #: ".$row['prodNbr']."\tProduct Name: "
                  .$row['prodName']."\tSupplier Number: ".$row['supplierNbr']."\tQuantity Available: "
                  .$row['qtyHand']."\tPrice: ".$row['price']."<br/>";
        }
    }
?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Vader's Emporium</title>
</head>

<body>  
    <p>
        <?php
            switch ($tableSelection)
            {
                case "customers":
                    $result = mysqli_query($con, $sqlDisplayCustomers);
                    displayCust($result);
                    break;
                case "orders":
                    $result = mysqli_query($con, $sqlDisplayOrders);
                    displayOrd($result);
                    break;
                case "products":
                    $result = mysqli_query($con, $sqlDisplayProducts);
                    displayProd($result);
                    break;
            }
        ?>
    </p>
    <p>
        <?php
            $result = mysqli_query($con, $sqlGetQty);
            if ($row = mysqli_fetch_array($result))
            {
                $newItemQty = intval($addThisMany + $row['qtyHand']);
                
                $sqlAddItems = "UPDATE products SET qtyHand = '$newItemQty' WHERE prodNbr = '$itemSelected'";
                
                if(mysqli_query($con, $sqlAddItems));
                    echo "We increased the quantity of the \"".$row['prodName']."\" item! There are now ".$newItemQty." unit(s).";
            }
            else
                echo "Sorry, we couldn't add to the quantity.";

        ?>
    </p>
</body>
</html>