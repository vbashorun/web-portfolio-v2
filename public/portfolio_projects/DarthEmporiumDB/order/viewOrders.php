<html>
<head>
  <title>Darth's Emporium - Customer Orders</title>
</head>
<body>
    <h1>Darth's Emporium</h1>
    <h2>Customer Orders</h2>
    <?php

        //database variables
        $dbHost = "localhost";
        $dbName = "vader";  
        $dbUser = "root";
        $dbPass = "password";
        $sqlShowOrders = "SELECT * FROM orders";

        //connect to database
        $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
        //check the connection
        if (mysqli_connect_errno())
            echo "Failed to connect to MySQL:".mysqli_connect_error();

        //select the database
        mysql_select_db ($dbName);

        if ($result = mysqli_query($con, $sqlShowOrders)) 
        {
            while ($row = mysqli_fetch_array($result))
            {
                echo "Order: ".$row['orderNbr']."\tDate: ".$row['orderDate']."\tCustomer: ".$row['custNbr']."\tProduct #: ".$row['prodNbr']."\tQuantity: ".$row['quantity']."<br/>";
            }
        }
        else
        {
            echo "<p><strong>No orders pending.
                  Please try again later.</strong></p>";
        }
    ?>
    
    <a href="/DarthEmporiumDB/catalog/catalog.php">Back to Catalog</a>
    
</body>
</html>