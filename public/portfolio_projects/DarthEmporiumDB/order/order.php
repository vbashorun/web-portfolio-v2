<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="">
    
    <script>
        function verifyOrder()
        {
            if (document.getElementById("0").value == "")
                return false;
            else if (document.getElementById("1").value == "")
                return false;
            else if (document.getElementById("2").value == "")
                return false;
            else if (document.getElementById("3").value == "")
                return false;
            else if (document.getElementById("4").value == "")
                return false;
            else
                return true;
        }
    </script>
</head>

<body>
    <div>
        <?php include '../header/header.php'; ?>
    </div>
    
    <form action="processOrder.php" method="post">
        <table>
            <tr><th colspan="2">Place Your Order</th></tr>

            <tr>
                <td>Light Saber</td>
                <td>Qty:<input id="0" type="input" size="5" name="saber"/></td>
            </tr>

            <tr>
                <td>Phaser</td>
                <td>Qty:<input id="1" type="input" size="5" name="phaser"/></td>
            </tr>

            <tr>
                <td>X-Wing</td>
                <td>Qty:<input id="2" type="input" size="5" name="wing"/></td>
            </tr>

            <tr>
                <td>Death Ray</td>
                <td>Qty:<input id="3" type="input" size="5" name="ray"/></td>
            </tr>

            <tr>
                <td>Walker</td>
                <td>Qty:<input id="4" type="input" size="5" name="walker"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Complete Order" onClick="verifyOrder()"/></td>
            </tr>
        </table>
    </form>
</body>
</html>
