<?php
    $saberQty = $_POST['saber'];
    $phaserQty = $_POST['phaser'];
    $wingQty = $_POST['wing'];
    $rayQty = $_POST['ray'];
    $walkerQty = $_POST['walker'];
    $date = date('H:i, jS F Y');

    /*
        Order table fields:
        orderNbr - int
        orderDate - date
        custNbr - int
        prodNbr - int
        quantity - int

        Product table fields:
        prodNbr - int
        prodName - text
        supplierNbr - int
        qtyHand - int
        price - float
    */

    //get user details from current user file
    $file = fopen("../users/currentUser.txt", "r");
    if (!file_exists ("../users/currentUser.txt")) 
        echo "File didn't open";
    flock($file, LOCK_SH); // read lock
    $userString = fgets($file);
    $tabPos = strpos($userString,"\t");
    $username = trim(substr($userString, $pos0,$tabPos));
    $password = trim(substr($userString, $tabPos + 1));
    flock($file, LOCK_UN);
    fclose($file);

    //database variables
    $dbHost = "localhost";
    $dbName = "vader";  
    $dbUser = "root";
    $dbPass = "password";
    $sqlSelectUser = "SELECT * FROM customer WHERE userid = '$username' AND password = '$password'";
    $sqlInsertOrder = "INSERT INTO orders (orderDate, custNbr, prodNbr, quantity) VALUES ('$date', '$custNbr', '$prodNbr','$quantity')";

    //connect to database
    $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
    //check the connection
    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL:".mysqli_connect_error();

    //select the database
    mysql_select_db ($dbName);

    //get customer info from database
    $result = mysqli_query($con, $sqlSelectUser);
    if ($row = mysqli_fetch_array($result))
        $custNbr = $row['custNbr'];

    //insert order
    if (!mysqli_query($con, $sqlInsertOrder))
        echo "<p><strong> Your order could not be processed at this time.
              Please try again later.</strong></p></body></html>";
    else
        echo "<p>Order written.</p>";
?>

<!DOCTYPE html>
<html>
<head>
    <title>Darth's Emporium - Processing Order</title>
</head>
<body>
    <h1>Darth's Emporium</h1>
    <h2>Order Results</h2>
    <p>
      <?php // assumes that the order was verified client-side

        echo "<p>Order processed at ".$date."</p>";
        echo "<p>Your order is as follows: </p>";

        //calculate the total number of items ordered
        $totalqty = $saberQty + $phaserQty + $wingQty + $rayQty + $walkerQty;
        echo "Items ordered: ".$totalqty."<br />";

        // verify order
        if ($totalqty == 0) 
        {
          echo "You did not order anything on the previous page!<br />";
        } 
        else 
        {
           if ($saberQty > 0) 
          {
            echo $saberQty." Light Saber(s)<br />";
            $quantity = $saberQty;
            $prodNbr = 00015;
            //insert order
            mysqli_query($con, $sqlInsertOrder);
          }

           if ($phaserQty > 0) 
          {
            echo $phaserQty." Phaser(s)<br />";
            $quantity = $phaserQty;
            $prodNbr = 00016;    
            //insert order
            mysqli_query($con, $sqlInsertOrder);
          }

            if ($wingQty > 0) 
          {
            echo $wingQty." X-wing(s)<br />";
            $quantity = $wingQty;
            $prodNbr = 00017;
            //insert order
            mysqli_query($con, $sqlInsertOrder);
          }

            if ($rayQty > 0) 
          {
            echo $rayQty." Death Ray(s)<br />";
            $quantity = $rayQty;
            $prodNbr = 00018;
            //insert order
            mysqli_query($con, $sqlInsertOrder);
          }
            if ($walkerQty > 0) 
          {
            echo $walkerQty." Walker(s)<br />";
            $quantity = $walkerQty;
            $prodNbr = 00019;
            //insert order
            mysqli_query($con, $sqlInsertOrder);
          }
            
            /*
                The insert statments will not work because the 'prodNbr' field is a foreign key referenced
                by another table. Without the product and its prodNbr also existing in that table, we cannot 
                insert the order for said product into the order's table
            */

        }

    /////////////////////////////////////////calculate price and prep data for file output///////////////////////////////////////////
        $totalamount = 0;

        define('SABERPRICE', 1000);
        define('PHASERPRICE', 500);
        define('WINGPRICE', 30000);
        define('RAYPRICE', 300000);
        define('WALKERPRICE', 70000);

        $totalamount = ($saberQty * SABERPRICE) + 
                       ($phaserQty * PHASERPRICE) + 
                       ($wingQty * WINGPRICE) + 
                       ($rayQty * RAYPRICE) + 
                       ($walkerQty * WALKERPRICE);

        //number formatting
        //$totalamount=number_format($totalamount, 2, '.', ' ');

        //email order to user
        $email = $row['email'];
        $address = $row['streetAddress']."\t".$row['city']."\t".$row['residentState']."\t".$row['zip'];

        echo "<p>Total of order is $".$totalamount."</p>";
        echo "<p>Address to ship to is ".$address."</p>";

        $outputstring = $date."\t".$saberQty." Light Saber(s)\t".$phaserQty." Phaser(s)\t".$wingQty." X-Wing(s)\t".$rayQty." Death Ray(s)\t".$walkerQty." Walker(s)\t\$".$totalamount."\t". $address."\n";

        //email order details to customer
        $subject = "Darth's Emporium Order Details";
        $fromaddress = "Darth@emporium.com";
        mail($email, $subject, $outputstring, $fromaddress);
    ?>
    </p>

    <p><a href="viewOrders.php">View orders </a></p>
</body>
</html>
