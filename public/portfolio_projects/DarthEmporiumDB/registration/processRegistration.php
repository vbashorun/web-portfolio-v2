<?php
    //helper vairables  
    $pos0 = 0; //don't want any hardcoded numbers
    $padLength = 500;
    $processError = "<p><strong> Your registration could not be processed at this time. Please try again later.</strong></p>";

    $fName = trim($_POST['fName']);
    $lName = trim($_POST['lName']);
    $username = trim($_POST['user']);
    $password = trim($_POST['pass']);
    $email = trim($_POST['email']);
    $street = trim($_POST['street']);
    $city = trim($_POST['city']);
    $state = trim($_POST['state']);
    $zip = intval(trim($_POST['zip']));
    $crntUser = trim($_POST['crntUser']);
    $crntPass = trim($_POST['crntPass']);

    //database variables
    $dbHost = "localhost";
    $dbName = "vader";  
    $dbUser = "root";
    $dbPass = "password";
    $sqlSelectUser = "SELECT userid, password FROM customer WHERE userid = '$crntUser' AND password = '$crntPass'";
    $sqlInsertUser = "INSERT INTO customer (fname, lname, userid, password, email, streetAddress, city, residentState, zip) VALUES ('$fName', '$lName', '$username', '$password', '$email', '$street', '$city', '$state', '$zip')";
    $sqlUpdateUser = "UPDATE customer SET fname = '$fName', lname = '$lName', userid = '$username', password = '$password', email = '$email', streetAddress = '$street', city = '$city', residentState = '$state', zip = '$zip' WHERE userid = '$username' AND password = '$crntPass'";

    //connect to database
    $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
    //check the connection
    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL:".mysqli_connect_error();

    //select the database
    mysql_select_db ($dbName);
      
    //check if post came in from "admin" file
    if ($rewrite = $_POST['rewrite']) //find the proper record and rewrite it's content
    {
        //sql code for updating user entry
        $result = mysqli_query($con, $sqlUpdateUser);
        if (!$result)
            die("Database access failed: " . mysql_error());
              
        echo "<p>Changes Postsed!</p><br/>"."<a href=\"/DarthEmporiumDB/admin/admin.php\">Return to Admin Page</a>";
        
        //store updated user and currently logged in user
        $updatedUser = $username."\t".$password;
        storeCurrentUser($updatedUser);
    }

    else //store new user in database
    {   
        if (!mysqli_query($con, $sqlInsertUser))
            die($processError);
        
        $userString = $username."\t".$password;
        
        storeCurrentUser($userString);
        
        //take user to catalog page
        header('Location:/DarthEmporiumDB/catalog/catalog.php');
    }

    ////////////////////////////////////log newly registered user as current user////////////////////////////////////////

    function storeCurrentUser($userString)
    {
        $file = fopen("../users/currentUser.txt", "w");

        if (!file_exists ("../users/users.txt")) 
            echo "File didn't open";

        flock($file, LOCK_EX); // write lock

        //pad string to control byte-length of each subsequent write to the file
        $userString = str_pad($userString, $padLength);
        fputs($file, $userString);

        flock($file, LOCK_UN);
        fclose($file);
    }
?>