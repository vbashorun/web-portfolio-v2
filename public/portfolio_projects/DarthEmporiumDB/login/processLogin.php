<?php
    //collect the information from the form that requested this page
    $username = trim($_POST["username"]);
    $password = trim($_POST["password"]);
    
    //database variables
    $dbHost = "localhost";
    $dbName = "vader";  
    $dbUser = "root";
    $dbPass = "password";
    $sqlSelectUser = "SELECT userid, password FROM customer WHERE userid = '$username' AND password = '$password'";

    //helper (misc) vairables
    $loginError = "Sorry, you're not a registered user. Please take the time to register now.";
    $padLength = 50;


    //connect to database
    $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
    //check the connection
    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL:".mysqli_connect_error();

    //select the database
    mysql_select_db ($dbName);

    //query the database for user info
    $result = mysqli_query($con,$sqlSelectUser);

    //break result set into array
    if($row = mysqli_fetch_array($result))
    {
        $userString = $row['userid']."\t".$row['password'];
        //store user as currently logged in
        storeCurrentUser($userString);
    }

    
    ////////////////////////////////////log newly registered user as current user////////////////////////////////////////
    function storeCurrentUser($userString)
    {
        $file = fopen("../users/currentUser.txt", "w");

        if (!file_exists ("../users/currentUser.txt")) 
            echo "File didn't open";

        flock($file, LOCK_EX); // write lock

        //pad string to control byte-length of each subsequent write to the file
        $userString = str_pad($userString, $padLength);
        fputs($file, $userString);

        flock($fp, LOCK_UN);
        fclose($file);

        //take user to next page
        header('Location:/DarthEmporiumDB/catalog/catalog.php');
    }
?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Log-In Page</title>
    
    <style> 
        div {
            margin-left:auto;
            margin-right:auto;
            width:40%;
        }
    </style>
</head>

<body>
    <div>
        <h4><?php echo $loginError; ?></h4>
        <a href="/DarthEmporiumDB/registration/registration.html">Register New User</a>
    </div>
</body>
</html>
