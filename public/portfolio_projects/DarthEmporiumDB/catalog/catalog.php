<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Catalog</title>

<script src="catalog.js"></script>
    
</head>

<body>
    <div>
        <?php include '../header/header.php'; ?>
    </div>
    
    <div id="wrapper">
        <h1 align="center">Item Catalog</h1>

        <p align="center">
            <img src="../images/item0-600.jpg" id="largeImage" width="600" height="600" alt="item_image" />
        </p>

        <div align="center" id="itemDetails">
            <h4 style="color:red">Name: <span id="itemName" style="color:black"></span></h4>
            <p style="color:red">Cost: <span id="itemPrice" style="color:black"></span></p>
            <p style="color:red">Description: <span id="itemDescription" style="color:black"></span></p>
        </div>

        <div align="center">
          <table width="794" border="1">
            <tr>
              <td><img src="../images/item0-200.jpg" name="0" width="200" height="200" onClick="picShift(this)" onMouseOver="hoverOver(this)" onMouseOut="hoverOut()"/></td>
              <td><img src="../images/item1-200.jpg" name="1" width="200" height="200" onClick="picShift(this)" onMouseOver="hoverOver(this)" onMouseOut="hoverOut()"/></td>
              <td><img src="../images/item2-200.jpg" name="2" width="200" height="200" onClick="picShift(this)" onMouseOver="hoverOver(this)" onMouseOut="hoverOut()"/></td>
              <td><img src="../images/item3-200.jpg" name="3" width="200" height="200" onClick="picShift(this)" onMouseOver="hoverOver(this)" onMouseOut="hoverOut()"/></td>
              <td><img src="../images/item4-200.jpg" name="4" width="200" height="200" onClick="picShift(this)" onMouseOver="hoverOver(this)" onMouseOut="hoverOut()"/></td>
            </tr>
          </table>
          <p>Hover over a thumbnail to make the larger version show, and click it to make it stay</p>
        </div>
    </div>
</body>
</html>