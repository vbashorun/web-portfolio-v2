var prevPic,
    hoverPic,
    clicked = false,
    itemQty = 5,
    picArray = new Array(itemQty),
    priceArray = [1000, 500, 30000, 300000, 70000],
    itemNames = ["Light Saber", "Phaser", "X-Wing", "Death Ray", "Walker"],
    descArray = [ "Whether Jedi or Dark Lord, your repetoire is absolutely CANNOT be complete without one of these bad boys!",
                  "Is a trooper hassling you for your lunch money? Whip out one of these and we can guarantee that he'll turn around and forget who you are.",
                  "Might not be a bad idea to have one of these around in case the next guy who presumptuously places \"Darth\" in front of his name happens to have a death star too. A prepared man is...well, a prepared man.",
                  "If you're planning on leading a planetary invasion, the other dark lords might not take you too seriously without your very own death ray. Heck, if you threatened my planet without one of these, it'd take everything in to hold back a chuckle. Just sayin\'.",
                  "Got an impending battle coming up with a group of annoying, rebellious Jedi? Get these guys to occupy your vanguard and you'll give those Jedi a run for their money. Hey, you might even just be able to keep your hands clean. That'd be neat huh?" ];

for (var i = 0; i < picArray.length; i++)
{
  picArray[i] = "../images/item" + i + "-600.jpg";
}

function picShift(element)
{
  var callerName = element.name;

  //This will allow the picture to stay and counteract the onMouseOut call
  clicked = true;
    
  document.getElementById("largeImage").src=picArray[Number(callerName)];
  document.getElementById("itemName").innerHTML=itemNames[Number(callerName)];
  document.getElementById("itemPrice").innerHTML= "$" + priceArray[Number(callerName)];
  document.getElementById("itemDescription").innerHTML=descArray[Number(callerName)];

}//END picShift()

function hoverOver(element)
{
  prevPic = document.getElementById("largeImage").src;
  picShift(element);
  clicked = false;
}

function hoverOut()
{
  if(clicked == false)
  document.getElementById("largeImage").src=prevPic;
}