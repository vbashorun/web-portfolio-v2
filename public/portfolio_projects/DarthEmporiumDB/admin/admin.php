<?php
    $pos0 = 0;

    //get user details from current user file
    $file = fopen("../users/currentUser.txt", "r");

    if (!file_exists ("../users/currentUser.txt")) 
        echo "File didn't open";
    
    flock($file, LOCK_SH); // read lock

    $userString = fgets($file);

    $tabPos = strpos($userString,"\t");
    
    $username = trim(substr($userString, $pos0,$tabPos));
    $password = trim(substr($userString, $tabPos + 1));

    flock($file, LOCK_UN);
    fclose($file);

    /////////////////////////////////////////////////Database Code////////////////////////////////////////////////////////////////

    //database variables
    $dbHost = "localhost";
    $dbName = "vader";  
    $dbUser = "root";
    $dbPass = "password";
    $sqlSelectUser = "SELECT * FROM customer WHERE userid = '$username' AND password = '$password'";

    //connect to database
    $con = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
    //check the connection
    if (mysqli_connect_errno())
        echo "Failed to connect to MySQL:".mysqli_connect_error();

    //select the database
    mysql_select_db ($dbName);

    //query the database for user info
    $result = mysqli_query($con,$sqlSelectUser);

    //break result set into array
    if($row = mysqli_fetch_array($result))
    {
        $username = $row['userid'];
        $password = $row['password'];
        $fName = $row['fname'];
        $lName = $row['lname'];
        $email = $row['email'];
        $street = $row['streetAddress'];
        $city = $row['city'];
        $state = $row['residentState'];
        $zip = $row['zip'];
        
    }
?>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Admin Page</title>
</head>

<body>
    <div>
        <?php include '../header/header.php'; ?>
    </div>
    
    <form action="/DarthEmporiumDB/registration/processRegistration.php" method="post" >
        <table>
            <tr>
                <td>User:</td>
                <td><input type="text" name="user" readonly value="<?php echo $username ?>" /></td>
            </tr>

            <tr>
                <td>Password:</td>
                <td><input type="text" name="pass" value="<?php echo $password ?>" /></td>
            </tr>
            
            <tr>
                <td>First Name:</td>
                <td><input type="text" name="fName" value="<?php echo $fName ?>" /></td>
            </tr>
            
            <tr>
                <td>Last Name:</td>
                <td><input type="text" name="lName" value="<?php echo $lName ?>" /></td>
            </tr>

            <tr>
                <td>Email:</td>
                <td><input type="text" name="email" value="<?php echo $email ?>" /></td>
            </tr>
            
            <tr>
                <td>Street Address:</td>
                <td><input type="text" name="street" value="<?php echo $street ?>" /></td>
            </tr>
            
            <tr>
                <td>City:</td>
                <td><input type="text" name="city" value="<?php echo $city ?>" /></td>
            </tr>
            
            <tr>
                <td>State:</td>
                <td><input type="text" name="state" value="<?php echo $state ?>" /></td>
            </tr>
            
            <tr>
                <td>Zip:</td>
                <td><input type="text" name="zip" value="<?php echo $zip ?>" /></td>
            </tr>
            
            <tr>
                <td colspan="2"><input type="submit" value="Submit Changes" /></td>
            </tr>
        </table>
        <input type="hidden" name="rewrite" value="true" />
        <input type="hidden" name="crntUser" value="<?php echo $username ?>" />
        <input type="hidden" name="crntPass" value="<?php echo $password ?>" />
    </form>
    
</body>
</html>
