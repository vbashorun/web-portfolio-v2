//global variables
var numImages = 8,
    clickedImageId1, //comparison variable 1
    clickedImageId2, //comparison variable 2
    currentState,
    hiddenCard = "https://dl.dropboxusercontent.com/u/88988430/Projects%20For%20Portfolio%20Site/Concentration%20Game/images/hidden.png",
    clearedCard = "https://dl.dropboxusercontent.com/u/88988430/Projects%20For%20Portfolio%20Site/Concentration%20Game/images/cleared.png",
    matchesFound,
    victoryMessage = "Congrats! You found all of the matches!",
    imgRef = "images/",
    mimeType = ".png",
    gameBoard,
    imageNames = [ "dribbble icon",
                   "facebook icon",
                   "instagram icon",
                   "linkedin icon",
                   "pinterest icon",
                   "soundcloud icon",
                   "tumblr icon",
                   "twitter icon" ];
    
//State variables
var SHOWING_NONE = "showing none",
    SHOWING_ONE = "showing one",
    SHOWING_TWO = "showing two",
    GAME_OVER = "game over";


    
function initialize() 
{  
    currentState = SHOWING_NONE;
    matchesFound = 0;
    gameBoard = [];
    
    var numsLeft = []; //used to keep track random numbers
    
    //attach the click handlers, create board, and index image files
    for(var k = 0; k < numImages * 2; k++)
    {
        document.getElementById(k).src = hiddenCard;
        
        var element = document.getElementById(k.toString());
        element.addEventListener("click", imageClicked);
        
        //keep reference number less than max names index
        if (k <= numImages - 1)
            numsLeft.push(imgRef + imageNames[k] + mimeType);
        else
            numsLeft.push(imgRef + imageNames[k - numImages] + mimeType);
    }    
                
    for (var i = 0; numsLeft.length !== 0; i++)
    {
        //choose the random number to place on the "board"
        var randNum = Math.floor(Math.random()*numsLeft.length);
        gameBoard.push(numsLeft[randNum]);
        numsLeft.splice(randNum, 1);
    }

}//END initialize

function imageClicked()
{ 
    switch(currentState)
    {
        case SHOWING_NONE:
            if (document.getElementById(this.id).src === hiddenCard)
            {
                //reveal the hidden image
                this.src = gameBoard[Number(this.id)];
                //store image id into comparison variable 1
                clickedImageId1 = this.id;
                //change the game state accordingly
                currentState = SHOWING_ONE;
            }
            break;
            
        case SHOWING_ONE:
            if (this.src === hiddenCard)
            {
                //reveal the hidden image
                this.src = gameBoard[Number(this.id)];
                //store image id into comparison variable 2
                clickedImageId2 = this.id;
                //change the game state accordingly
                currentState = SHOWING_TWO;
                //start timer for comparison handling
                window.setTimeout("timeOutHandler()", 1500);
            }
            break;      
    }
}//END imageClicked

function timeOutHandler()
{
    var pic1 = document.getElementById(clickedImageId1),
        pic2 = document.getElementById(clickedImageId2);

    //check if cards match
    if (pic1.src === pic2.src)
    {
        //clear cards off of board
        pic1.src = clearedCard;
        pic2.src = clearedCard;
        matchesFound++;
    }
    else
    {
        //rehide cards
        pic1.src = hiddenCard;
        pic2.src = hiddenCard;
    }
    
    //check for game progress
    if (matchesFound === numImages)
    {
        currentState = GAME_OVER;
        alert(victoryMessage);
    }
    else
        currentState = SHOWING_NONE;     
}

function showMatches()
{
    for(var k = 0; k < numImages * 2; k++)
        document.getElementById(k).src = gameBoard[k];
}