var prevPic,
	  hoverPic,
	  clicked = false,
	  picArray = new Array();

  // change these pic names
  picArray[0] = "images/ireland1-600.jpg";
  picArray[1] = "images/ireland2-600.jpg";
  picArray[2] = "images/ireland3-600.jpg";
  picArray[3] = "images/ireland4-600.jpg";
  
  function picShift(element)
  {
	  var callerName = element.name;
	  
	  //This will allow the picture to stay and counteract the onMouseOut call
	  clicked = true;
	  
	  switch (callerName)
	  {
		  case "ire_1":
		    document.getElementById("largeImage").src=picArray[0]; 
			break;
		  
		  case "ire_2":
		    document.getElementById("largeImage").src=picArray[1];
			break;
		  
		  case "ire_3":
		    document.getElementById("largeImage").src=picArray[2];
			break;
		  
		  case "ire_4":
		    document.getElementById("largeImage").src=picArray[3];
			break;
	  }
  }//END picShift()
  
  function hoverOver(element)
  {
	  prevPic = document.getElementById("largeImage").src;
	  picShift(element);
	  clicked = false;
  }
  
  function hoverOut()
  {
	  if(clicked == false)
	  document.getElementById("largeImage").src=prevPic;
  }